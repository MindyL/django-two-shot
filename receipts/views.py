from django.shortcuts import render, redirect

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    # success_url = reverse_lazy("home")

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")

    # below function allow user to see categories and accounts CREATED BY USER ONLY
    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, *kwargs)
        form.fields["category"].queryset = self.request.user.categories.all()
        form.fields["account"].queryset = self.request.user.accounts.all()
        return form


# Receipt createview in functions
# def create_receipt(request):
#     if request.method == “POST”:
#         form = ReceiptForm(request.POST)
#         if form.is_valid():
#             receipt = form.instance
#             receipt.purchaser = request.user
#             receipt.save()
#             return redirect(“home”)
#     else:
#         form = ReceiptForm()
#         form.fields[“category”].queryset = request.user.categories.all()
#         form.fields[“account”].queryset = request.user.accounts.all()
#     context = {“form”: form}
#     return render(request, “receipts/new_receipt.html”, context)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/category_list.html"
    # context_object_name = "expensecategory_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/account_list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/newcategory.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("category_list")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/newaccount.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("account_list")
