from django.shortcuts import render, redirect

# Create your views here.

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()

    context = {"form": form}
    return render(request, "registration/signup.html", context)


# from django.views.generic.edit import CreateView
# from django.urls import reverse_lazy


# class SignUpView(CreateView):
#     form_class = UserCreationForm
#     success_url = reverse_lazy("home")
#     template_name = "registration/signup.html"
